var tellnode = require('./index');

tellnode.on('device', function(device) {
	console.log('event device: ' + JSON.stringify(device));
});

tellnode.on('sensor', function(sensor) {
	console.log('event sensor: ' + JSON.stringify(sensor));
});

console.log('devices: ' + JSON.stringify(tellnode.getDevices()));

console.log('turning on 3');
tellnode.turnOn(3);
console.log('-');

setTimeout(function() { tellnode.turnOn(3); }, 3000);
