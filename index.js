/**
 *
 * tellnode.cc

 CreateDevice
 ------------
 id
 name
 type
 value

 SensorEvent
 -----------
 protocol
 sensorId
 model
 temperature
 humidity
 time

 DeviceEvent
 -----------
 deviceId
 lasttime
 callbackId
 method?
 command ( TURN_ON, TURN_OFF, DIM, etc )
 prot
 house
 unit
 dimvalue (id command = DIM)

 */

var tellnode = require('./lib/tellnode');
var events = require('events');

var event = new events.EventEmitter();

var sensors = [];

var sensorCache = {};

function getCachedSensor(id) {
	return sensorCache[id];
}

function copySensor(sensor) {
	return {
		sensorId: sensor.sensorId,
		protocol: sensor.protocol,
		model: sensor.model,
		temperature: sensor.temperature,
		humidity: sensor.humidity,
		time: sensor.time
	}
}

function updateCache(sensor) {

	var cachedSensor = getCachedSensor(sensor.id);

	if (cachedSensor === undefined) {
		cachedSensor = copySensor(sensor);
	}

	if (sensor.humidity !== undefined) {
		cachedSensor.humidity = sensor.humidity;
	} else {
		cachedSensor.temperature = sensor.temperature;
	}
}

function contains(source, id) {
  for (var i = 0; i < source.length; i++) {
    if (source[i].id === id) {
      return true;
    }
  }
  return false;
}

tellnode.addDeviceListener(function(device) {
	event.emit('device', device);
});

tellnode.addSensorListener(function(sensor) {
	if (!contains(sensors, sensor.sensorId)) {
		sensors.push(sensor);
	}

	updateCache(sensor);

	event.emit('sensor', sensor);
});

module.exports.getDevices = function() {
	return tellnode.getDevices();
}

module.exports.on = function(listener, param) {
	event.on(listener, param);
}

module.exports.turnOn = function(id) {
	tellnode.turnOn(id);
}

module.exports.turnOff = function(id) {
	tellnode.turnOff(id);
}

module.exports.getSensors = function() {
	return sensors;
}
